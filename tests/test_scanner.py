from ..common.scanner import *
from ..common.file import File
from random import randint

def test_dir_scan(tmp_path):
    file = tmp_path / "tmpdir/tmpfile.txt"
    file.parent.mkdir()
    file.touch()

    assert dir_scan(tmp_path) == [f"{tmp_path}/tmpdir"]
    assert dir_scan(f"{tmp_path}/tmpdir") == [str(file)]


def test_file_scan(tmp_path):
    file = tmp_path / "tmpdir/tmpfile.txt"
    file.parent.mkdir()
    file.touch()

    assert file_rscan(tmp_path) == [str(file)]


def test_file_scan_recursive(tmp_path):
    file = tmp_path / "tmpdir1/tmpdir2/tmpfile.txt"
    file.parent.parent.mkdir()
    file.parent.mkdir()
    file.touch()

    assert file_rscan(tmp_path) == [str(file)]


def test_file_scan_multiple_files(tmp_path):
    file1 = tmp_path / "tmpfile1.txt"
    file2 = tmp_path / "tmpfile2.txt"
    file3 = tmp_path / "tmpfile3.txt"

    files = [file1, file2, file3]
    [file.touch() for file in files]

    assert set(file_rscan(tmp_path)) == set(str(file) for file in files)


def test_file_multithreaded_file_scan(tmp_path):
    file1 = tmp_path / "tmpfile1.txt"
    file2 = tmp_path / "tmpfile2.txt"

    files = [file1, file2]
    [file.touch() for file in files]

    scanned_files = scan(directory=tmp_path, threads=randint(2, 10))
    hashed_file1 = File(file1)
    hashed_file2 = File(file2)

    assert len(scanned_files) == 2
    assert hashed_file1.data in scanned_files
    assert hashed_file2.data in scanned_files