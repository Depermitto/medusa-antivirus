from ..common.extract import *

def test_extract_zip(tmp_path):
    file = tmp_path / "tmpfile.txt"
    file.touch()
    file.write_text("Hello World")
    
    assert unzip(file) == []

    file_zipped = ZipFile(f"{file}.zip", "w")
    file_zipped.write(file)
    file_zipped.close()

    assert unzip(f"{file}.zip") == [f"./{TMP_DIR}/{file}.zip-unzipped-medusa{file}"]


def test_extract_zip_recursive(tmp_path):
    file = tmp_path / "tmpfile.txt"
    file.touch()
    file.write_text("Hello World")

    assert unzip(file) == []

    file_zipped = ZipFile(f"{file}.zip", "w")
    deep_file_zipped = ZipFile(f"{file}.zip.zip", "w")
    deep_file_zipped.write(file)
    deep_file_zipped.close()
    file_zipped.write(f"{file}.zip.zip")
    file_zipped.close()

    assert unzip(f"{file}.zip") == [f"./{TMP_DIR}/{file}.zip-unzipped-medusa{file}.zip.zip"]
    assert runzip(f"{file}.zip") == [f"./{TMP_DIR}/./{TMP_DIR}/{file}.zip-unzipped-medusa{file}.zip.zip-unzipped-medusa{file}"]