from ..common.database_communicator import DatabaseCommunicator as DC
from ..common.file import File

def test_dc_init():
    remote = DC(dbname="medusa", user="virusguest", password="123123123",
                   host="172.104.202.242", default_table="small_hashes")
    
    assert remote._dbname == "medusa"
    assert remote._user == "virusguest"
    assert remote._password == "123123123"
    assert remote._host == "172.104.202.242"
    assert remote.table == "small_hashes"


def test_dc_match_hashes_single_file():
    remote = DC(dbname="medusa", user="virusguest", password="123123123",
                   host="172.104.202.242", default_table="small_hashes")

    file = File("./tests/test_dc.py")
    assert remote.match_hashes([file.data]) == [{}]

def test_dc_match_hashes_multiple_files():
    remote = DC(dbname="medusa", user="virusguest", password="123123123",
                   host="172.104.202.242", default_table="small_hashes")

    file1 = File("./tests/test_dc.py")
    file2 = File("./tests/test_file.py")
    file3 = File("./tests/__init__.py")

    assert remote.match_hashes([file1.data, file2.data, file3.data]) == [{}]


def test_dc_match_hashes_virus():
    remote = DC(dbname="medusa", user="virusguest", password="123123123",
                   host="172.104.202.242", default_table="small_hashes")

    file: dict = File("./tests/test_dc.py").data
    virus: dict = {
        "name": "/path/to/virus",
        "hash": "44d88612fea8a8f36de82e1278abb02f"
    }

    assert remote.match_hashes([file, virus]) == ["./path/to/virus"]
