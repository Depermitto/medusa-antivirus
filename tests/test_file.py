from ..common.file import File

from pytest import raises

def test_file_init(tmp_path):
    tf = tmp_path / "tmpfile.txt"
    tf.touch()

    tf.write_text("Hello World")
    file = File(tf)

    assert file.hash == "b10a8db164e0754105b7a99be72e3fe5"
    assert file.size == 0.000011
    assert file._last_mod_posix > 0
    assert type(file.last_mod) == str
    assert type(file.name) == str


def test_file_init_empty(tmp_path):
    tf = tmp_path / "tmppath"
    tf.touch()

    tf.write_text("")
    assert tf.read_text() == ""

    file = File(tf)
    assert file.hash == ""


def test_file_init_big(tmp_path):
    tf = tmp_path / "tmpfile.txt"
    tf.touch()

    tf.write_text("r" * int(3e8))
    file = File(tf)

    assert file.size == 300
    assert file.hash == ""


def test_file_init_non_existant(tmp_path):
    with raises(FileNotFoundError):
        File(tmp_path / "tmpfile.txt")


def test_file_str(tmp_path):
    tf = tmp_path / "tmpfile.txt"
    tf.touch()

    tf.write_text("Hello World")
    file = File(tf)

    assert str(file) == f"{file.name}, b10a8db164e0754105b7a99be72e3fe5"