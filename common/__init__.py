import json
import os

INDEX_FILE: str = "index.json"
LOGS_DIR: str = "logs"
LOGS_FILE: str = "medusa.log"
TMP_DIR: str = "tmp"

# Create empty index.json
if not os.path.exists(INDEX_FILE):
    with open(INDEX_FILE, "w") as index_file:
        index_file.write("{}")

with open(INDEX_FILE) as index_file:
    INDEX: dict[str, dict] = json.load(index_file)


# Create logs directory
if not os.path.exists(f"./{LOGS_DIR}/"):
    os.mkdir(f"./{LOGS_DIR}")


# Create tmp directory
if not os.path.exists(f"./{TMP_DIR}/"):
    os.mkdir(f"./{TMP_DIR}")