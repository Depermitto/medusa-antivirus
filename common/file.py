import hashlib
from datetime import datetime
from os.path import (
    getsize as size,
    getmtime as last_mod,
    abspath
)

class File:
    """File Object. Hashes the file and holds multiple properties regarding the file,
    such as: it's hashes content or  time and date of last modification.
    """
    def __init__(self, path: str) -> None:
        self._relative_path: str = path
        self._raw_size: int = size(path)
        self._last_mod_posix: float = last_mod(path)

        # Hashes the file and returns the hash.
        if 0 < self._raw_size < 3e8:
            with open(path, "rb") as opened_file:
                # _Hash object creation.
                md5 = hashlib.md5()
                content = opened_file.read()
                md5.update(content)
                opened_file.close()
            self._hash: str = md5.hexdigest()
        else:
            self._hash: str = ""

    @property
    def name(self):
        """Absolute path to the file,
        path and name will be used interchangeably."""
        return abspath(self._relative_path)

    @property
    def hash(self) -> str:
        """Hashed content of the file."""
        return self._hash

    @property
    def size(self) -> float:
        """File size, in mb."""
        return self._raw_size / 1e6

    @property
    def last_mod(self) -> str:
        """Time of last modification in YEAR/MONTH/DAY HOUR:MINUTES:SECONDS."""
        return datetime.fromtimestamp(self._last_mod_posix).strftime(f"%Y/%m/%d %H:%M:%S")

    @property
    def data(self) -> dict:
        """All the atributes of the File in a readable dictonary."""
        return {
            "name": self.name,
            "hash": self.hash,
            "size": self.size,
            "last_mod": self.last_mod,
        }

    def __str__(self) -> str:
        return f"{self.name}, {self.hash}"