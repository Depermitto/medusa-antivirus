import os
import re
from zipfile import is_zipfile, ZipFile, BadZipFile
from shutil import rmtree

from .logger import update_log
from .__init__ import TMP_DIR

def unzip(path: str) -> list:
    """Unzips a .zip file and returns the contents. If path isn't a
    valid zipfile, empty or beyond 300mb in size, the returned value will be [].

    Args:
        path (str): Absolute or relative path to the file.add()

    Returns:
        list: List of paths to files/directories in the zipfile. The returned paths
        are generated with a pattern "./{TMP_DIR}/{path}-unzipped-medusa" where
        TMP_DIR is currently tmp.
    """
    try:
        if 0 < os.path.getsize(path) < 3e7 and is_zipfile(path):
            with ZipFile(path, "r") as opened_zip:
                tmp_path = f"./{TMP_DIR}/{path}-unzipped-medusa"
                update_log(f"Extracted into '{tmp_path}'.<br>", timestamp=True)
                opened_zip.extractall(tmp_path)
                return [f"{tmp_path}/{file}" for file in opened_zip.namelist() if not os.path.isdir(file)]
        else:
            return []
    except (BadZipFile, FileNotFoundError, OSError):
        return []

def runzip(path: str) -> list:
    """Same as unzip but recursive and returned paths are generated with
    a different pattern:

        R = number of recursive dips in the zipfile.

    path_to_file: "(./{TMP_DIR}/) * R / ({path}-unzipped-medusa) * R"""
    files = []
    for file in unzip(path):
        if is_zipfile(file):
            files += runzip(file)
        else:
            files.append(file)

    return files

def find_archive(path: str) -> str:
    """Run on unzipped-medusa files to find the original archive (.zip file) name.
    Works on returned path_to_file by unzip and runzip. Uses a combination of
    string magic and regex to accomplish this."""

    def _delete_slashes(string: str) -> str:
        if string[0].isalpha() or string[1:5] == "home":
            return string
        else:
            return _delete_slashes(string[1:])

    def _add_relative_prefix(string: str) -> str:
        if string[0] == "/":
            return string
        else:
            return f"./{string}"
    try:
        unzipped_medusa_file = re.search(f"/{TMP_DIR}/(.+?)-unzipped-medusa", path).group(1) # type: ignore
    except AttributeError:
        unzipped_medusa_file = path

    unzipped_medusa_file = unzipped_medusa_file.replace(f"./{TMP_DIR}/", "").replace(TMP_DIR, "")
    return _add_relative_prefix(_delete_slashes(unzipped_medusa_file))

def tmp_cleanup():
    """Clears the ./{TMP_DIR}"""
    rmtree(f"./{TMP_DIR}/")
    os.mkdir(f"./{TMP_DIR}/")
