import json
from os.path import getmtime
from datetime import datetime

from .file import File
from .logger import update_log
from .__init__ import *

def update_index(content: dict[str, dict]) -> None:
    """Forceful updating of INDEX with a single entry"""
    INDEX.update(content)

    with open(INDEX_FILE, "w") as index_file:
        json.dump(INDEX, index_file, indent=4)
    update_log(f"Updated a single entry.<br>", timestamp=True)

def multi_update_index(content: list[dict[str, dict]]) -> None:
    """Forceful updating of INDEX with multiple entries."""
    for dict in content:
        INDEX.update(dict)

    with open(INDEX_FILE, "w") as index_file:
        json.dump(INDEX, index_file, indent=4)
    update_log(f"Updated index.<br>", timestamp=True)

def cr_entry(path: str) -> dict[str, dict]:
    entry: dict[str, dict] = {path: File(path).data}
    update_log(f'Created entry "{entry}".<br>', timestamp=True)
    return entry

def cr_dicts(files: list[str]):
    """Creation of dicts out of files in INDEX."""
    for file in files:
        last_mod = datetime.fromtimestamp(getmtime(file)).strftime(f"%Y/%m/%d %H:%M:%S")

        if not INDEX.get(file):
            yield cr_entry(file)
        elif INDEX[file]["last_mod"] != last_mod:
            yield cr_entry(file)
        else:
            yield {file: INDEX[file]}
