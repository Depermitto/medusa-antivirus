import psycopg2
from psycopg2.extras import DictCursor

from .logger import update_log
from .extract import find_archive

class DatabaseCommunicator:
    """Communicates with PostgreSQL for Database manipulation.

    Atributes:
        _dbname (str): Name of the database.
        _user (str): Defaults to postgres.
        _host (str): Defaults to localhost.
        cursor (_Cursor): Database's cursor for SQL.
        table (str): Convinient shortcut for a table to use in methods. Defaults to public.virus_share.

    Methods:
        execute (command): Execute a command in SQL and save it to cursor.
        match_hashes (files, table): Checks if files' hashes are in table.
    """
    def __init__(self, dbname: str, default_table: str, user: str = "postgres",
                 host: str = "localhost", password: str = "") -> None:
        self._dbname = dbname
        self._host = host
        self._user = user
        self._password = password
        self._table = default_table
        self._conn = psycopg2.connect(dbname=dbname, user=user, host=host, password=password)
        self._cursor = self._conn.cursor(cursor_factory=DictCursor)

    @property
    def cursor(self):
        return self._cursor

    @property
    def table(self) -> str:
        return self._table

    def set_table(self, table: str) -> None:
        self._table = table

    def execute(self, command = None) -> None:
        """Executes 'command' in SQL on Database

        Args:
            command (optional): Command to execute. Defaults to "SELECT * FROM {self.table}"
        """
        if not command:
            self.cursor.execute(f"SELECT * FROM {self.table}")
        else:
            self.cursor.execute(command)

    def _parse_IN(self, iterable: list[str]):
        result: str = ""

        for index, element in enumerate(iterable):
            # Checking if is the last element
            if (index + 1) != len(iterable):
                result += f"'{element}',"
            else:
                result += f"'{element}'"

        return result

    def match_hashes(self, files: list[dict], table: (str | None) = None) -> (list[str] | list[dict]):
        """Checks whether 'files'' hashes are in table in the database. All arguments are expected
        to be consistent with psycopg2 standards.

        Args:
            files (list[dict]): List of files' data dicts.
            table (str | None): Defaults to self.default_table.

        Returns:
            (list[dict]): List of matched 'files'' data dicts.
        """
        hashes: list[str] = [file["hash"] for file in files]
        update_log("Found hashes...<br>", timestamp=True)
        table = self.table if not table else table

        update_log("Matching hashes...<br>", timestamp=True)
        self.execute(f"SELECT hash FROM {table} WHERE hash IN ({self._parse_IN(hashes)})")
        update_log("Matched hashes!<br>", timestamp=True)

        matched_hashes: list[str] = [h[0] for h in self.cursor.fetchall()]
        viruses: list[str] = [file["name"] for file in files if file["hash"] in matched_hashes]
        viruses = list(set([find_archive(virus) for virus in viruses]))

        return [{}] if len(viruses) == 0 else viruses


class DCFromFile:
    def __init__(self, database_file: str) -> None:
        self._database_file = database_file

    @property
    def database_file(self):
        return self._database_file

    def match_hashes(self, files: list[dict]):
        update_log("Found hashes...<br>", timestamp=True)
        update_log("Matching hashes...<br>", timestamp=True)
        file = open(self.database_file, "r")
        content = file.read(); file.close()

        viruses = [file["name"] for file in files if file["hash"] in content and file["hash"] != ""]
        update_log("Matched hashes!<br>", timestamp=True)
        viruses = list(set([find_archive(virus) for virus in viruses]))

        return [{}] if len(viruses) == 0 else viruses