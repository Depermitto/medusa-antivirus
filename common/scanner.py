import os

from threading import Thread
from math import ceil
from zipfile import is_zipfile
from typing import Generator

from .extract import runzip
from .logger import update_log, clear_log
from .index import cr_dicts, multi_update_index

clear_log()

def dir_scan(directory: str) -> list[str]:
    """Returns content of 'directory' in a form of a list of paths"""
    listdir = os.listdir(directory)
    dirs = [f"{directory}/{d}" for d in listdir if not os.path.isfile(d)]

    return dirs

def file_rscan(directory: str) -> list[str]:
    discovered_files = []

    for dirpaths, dirs, files in os.walk(directory):
        # Checking for already exported zips.
        if not "-unzipped-medusa" in dirpaths:
            for file in files:
                path_to_file = f"{dirpaths}/{file}"
                update_log(f"Descended into '{path_to_file}'.<br>", timestamp=True)
                # Search inside zip archives.
                if is_zipfile(path_to_file):
                    discovered_files += runzip(path_to_file)
                else:
                    discovered_files.append(path_to_file)

    return discovered_files

def provision(directory: str, workers: int) -> Generator:
    """Provisions discovered files by file_rscan by the number of 'workers'."""
    files = file_rscan(directory)
    chunk_size: int = ceil(len(files) / workers)

    for index in range(0, len(files), chunk_size):
        yield files[index:index + chunk_size]

def scan(directory: str, threads: int = 1) -> list[dict]:
    """Launches 'threads' amount of concurrent hashing processes. Very fast and CPU intensive
    if used with a high amount of threads. Defaults to 1 thread. Takes into consideration index.json
    and thanks to that, it gets faster with every scan as the database builds up."""
    discovered_dicts: list = []
    snakes: list[Thread] = []

    def _discover_dicts(files: list[str]) -> None:
        for dict in cr_dicts(files):
            discovered_dicts.append(dict)

    def _update_and_return(discovered_dicts: list[dict[str, dict]]) -> Generator:
        multi_update_index(discovered_dicts)

        for dict in discovered_dicts:
            yield list(dict.values())[0]

    for file_list in provision(directory, threads):
        snakes.append(Thread(target=_discover_dicts, args=[file_list], daemon=True))

    for snake in snakes:
        snake.start()

    for snake in snakes:
        snake.join()

    return list(_update_and_return(discovered_dicts))