from datetime import datetime

from .__init__ import LOGS_DIR, LOGS_FILE

def update_log(string: str, timestamp: bool = False):
    """Adds an entry to ./{LOGS_DIR}/{LOGS_FILE} in the format:
    [timestamp]     string

    timestamp is represented as HOUR/MINUTE/SECONDS"""
    if timestamp:
        text: str = datetime.now().strftime(f"[%H:%M:%S]\t") + string + "\n"
    else:
        text: str = string + "\n"

    with open(f"{LOGS_DIR}/{LOGS_FILE}", "a") as opened_log:
        opened_log.write(text)

def clear_log():
    """Clears content from ./{LOGS_DIR}/{LOGS_FILE}"""
    with open(f"{LOGS_DIR}/{LOGS_FILE}", "w") as opened_log:
        opened_log.write("")
