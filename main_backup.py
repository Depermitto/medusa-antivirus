from ui_medusa import *
from common import scanner
from common.logger import *
from common.database_communicator import DatabaseCommunicator, DCFromFile
from common.extract import tmp_cleanup

from send2trash import send2trash

class ScanThread(QtCore.QThread):
    def __init__(self, parent: QtWidgets.QMainWindow) -> None:
        super().__init__(parent)
        self.parent = parent # type: ignore
        self.FILE_DC = DCFromFile("small-virus-share.csv")

    def run(self) -> None:
        discovered_files: list[dict] = scanner.scan(self.parent.directory,
                                                         self.parent.ui.threadsSpinBox.value())
        viruses: list[str] = self.FILE_DC.match_hashes(files=discovered_files) # type: ignore

        for virus in viruses:
            if virus == {}:
                update_log(f"""<b style="color:LightGreen;">Found no suspicious files!</b><br>""", timestamp=True)
            else:
                try:
                    send2trash(virus)
                    update_log(f"""<b style="color:Tomato;">Trashed a suspicious file </b>'{virus}'.<br>""", timestamp=True)
                except:
                    update_log(f"""<b>File has been marked as suspicious, but couldn't be deleted </b>'{virus}'.<br>""", timestamp=True)


class MedusaWindow(QtWidgets.QMainWindow):
    def __init__(self) -> None:
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.logsTextBrowser.setSource(QtCore.QUrl("./logs/medusa.log"))

        self.directory = "."
        self.ui.directoryButton.clicked.connect(self.fileDialogPrompt)
        self.ui.directoryButton.clicked.connect(self.changeDirectoryButtonLabel)

        self.cyclingTimer = QtCore.QTimer(self)
        self.cyclingTimer.timeout.connect(self.scanningInProgress)
        self.cyclingTimer.timeout.connect(self.createCyclingScanThread)
        self.cyclingTimer.timeout.connect(self.changeScanNowButtonScanningLabel)

        self.ui.scanNowButton.clicked.connect(self.createScanThread)
        self.ui.scanNowButton.clicked.connect(self.changeScanNowButtonScanningLabel)
        self.ui.scanNowButton.clicked.connect(self.interruptCyclingTimer)

        self.ui.setupCyclingScanButton.clicked.connect(self.cyclingTimerSetup)

    def cyclingTimerSetup(self) -> None:

        def __cyclingTimerRestart() -> None:
            self.cyclingTimer.stop()
            self.cyclingTimer.setInterval(self.ui.everyMinutesSpinBox.value() * 1000 * 60)
            self.cyclingTimer.start()
            self.changeSetupCyclingScanButtonLabel()

        if hasattr(self, "scanThread"):
            if not self.scanThread.isRunning():
                __cyclingTimerRestart()
            else:
                self.ui.setupCyclingScanButton.setText("Cannot set up scheluding while scanning.")
        else:
            __cyclingTimerRestart()

    def changeSetupCyclingScanButtonLabel(self) -> None:

        def __changeSetupCyclingScanButtonRemainingTimeLabel():
            self.ui.setupCyclingScanButton.setText(f"Next scan in: {round(self.cyclingTimer.remainingTime() / 1000, 0)}s.")

        self.countdownTimer = QtCore.QTimer(self)
        self.countdownTimer.setInterval(1000)
        __changeSetupCyclingScanButtonRemainingTimeLabel()
        self.countdownTimer.timeout.connect(__changeSetupCyclingScanButtonRemainingTimeLabel)
        self.countdownTimer.start()

    def interruptCyclingTimer(self) -> None:
        if self.cyclingTimer.remainingTime() > 0:
            self.countdownTimer.stop()
            self.cyclingTimer.stop()
            self.ui.setupCyclingScanButton.setText(f"Stopped. Click again to set up scheduling.")

    def scanningInProgress(self) -> None:
        self.countdownTimer.stop()
        self.ui.setupCyclingScanButton.setText("Scanning in progress...")

    def changeScanNowButtonScanningLabel(self) -> None:
        self.ui.scanNowButton.setText("Scanning...")

    def changeScanNowButtonFinishedLabel(self) -> None:
        self.ui.scanNowButton.setText("Check logs for results.")

    def changeDirectoryButtonLabel(self) -> None:
        self.ui.directoryButton.setText(self.directory)

    def createScanThread(self) -> None:
        self.scanThread = ScanThread(self)
        self.scanThread.finished.connect(self.ui.logsTextBrowser.reload)
        self.scanThread.finished.connect(self.changeScanNowButtonFinishedLabel)
        self.scanThread.start()

    def createCyclingScanThread(self) -> None:
        self.scanThread = ScanThread(self)
        self.scanThread.finished.connect(self.ui.logsTextBrowser.reload)
        self.scanThread.finished.connect(self.changeScanNowButtonFinishedLabel)
        self.scanThread.finished.connect(self.cyclingTimerSetup)
        self.scanThread.start()

    def fileDialogPrompt(self) -> None:
        self.directory= QtWidgets.QFileDialog.getExistingDirectory(self,
            self.tr("Open Directory"), ".")


def main():
    import sys

    tmp_cleanup()
    app = QtWidgets.QApplication(sys.argv)
    medusa = MedusaWindow()
    medusa.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()