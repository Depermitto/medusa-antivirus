# Medusa
* [What is Medusa?](#what-is-medusa)
* [Getting started](#getting-started)
	* [Button Layout](#button-layout)
* [How does Medusa work?](#how-does-medusa-work)
* [How was Medusa built?](#how-was-medusa-built)
	* [\_\_init__.py](#__init__py)
	* [database_communicator.py](#database_communicatorpy)
	* [extract.py](#extractpy)
	* [file.py](#filepy)
	* [scanner.py](#scannerpy)
* [License](#license)
* [Medusa doesn't work!](#medusa-doesnt-work)

## What Is Medusa
> It should be noted that Medusa could be inaccurate or rather, produce false positives. Keep that in mind if you plan on using the software.

Medusa is a rather limited antivirus written in python. It works cross-platform but requires you to install the necessary python dependencies:
* [psycopg2](https://pypi.org/project/psycopg2/)
* [PyQt5](https://pypi.org/project/PyQt5/)
* [send2trash](https://pypi.org/project/Send2Trash/)

To install them either follow the instructions in the links or:
```
python -m pip install [dependency]
```

## Getting Started
> Make sure to take care of any missing dependencies beforehand. 

Simply clone the repo with:
```bash
git clone https://gitlab.com/Depermitto/medusa-antivirus
```
And execute **main.py**:
```bash
python3 main.py
```

If everything went right, the program should open correctly. You will be greeted with 3 buttons, 2 spin boxes and a slider.

### Button Layout
1. Before clicking anything else you should press the **Choose Directory** button. It will prompt you to select a *dir*. That directory will be used as the starting point for the scan thread. Every subdirectory and files inside them **will** also be discovered as the process is recursive.
> If empty, Medusa will scan the current working directory. 
2. **Amount of threads** is a spinbox connected to a slider. It indicates the speed at which Medusa is going to run. The higher the value, the faster the operation, but the more load your CPU will suffer.
> Min value is 1 and max is 30.
3. Pièce de résistance. The **Scan Now** button. It starts a new scan thread, scanning every *subdir* and *file* inside the chosen directory at the specified speed. Label on the button will be changed to ***Scanning***, indicating a running process. When finished, go to the **logs** tab, where lie logs of every activity done by Medusa.

4. Last but not least, the **Setup Cycling Scan** button. Upon activating the button, a scan thread is going to start in **X Minutes**. After the timeout the timer is going to restart, thus setting up an infinite cycling loop.
> The button will do nothing if *Scanning* and will be canceled when the **Scan Now** button is clicked.

## How Does Medusa Work?
> Medusa is a rather simple program, but can get convoluted at times. This section is meant to help you understand the intentions of the *author*.

This is the workflow of the program:
1. Scan the chosen directory and return a `list[str]` of absolute paths to the discoveries.
2. Check if a scanned file information is already in **index.json**.
3. Create instances of `File` from the files that are not in **index.json**. `File` holds that file's *last modification date* and its *hash value*. The most important attribute is the `data` as it holds every other property in the form of a `dict`.
4. Populate **index.json** with these `dicts` to save time in the future.
5. Send every file's dict to `DatabaseCommunicator` to check if that file is a virus. `DatabaseCommunicator` communicates with an outside PostgreSQL database to quickly filter out any not-matching records against an ton of data.
6. Returned values are marked as suspicious and promptly trashed.

## How Was Medusa Built?
> Details are in the source code. These are only snippets to give you an idea.

Medusa was built using Python 3.11 and it's highly recommend to run it under that version. For the most part, builtin python libraries were used, as well as PyQT5 for the GUI.

The **meat and potatoes** lie in the *common* directory.
```
medusa-antivirus
+-- common
|   +-- __init__.py
|   +-- database_communicator.py
|   +-- extract.py
|   +-- file.py
|   +-- index.py
|   +-- logger.py
|   +-- scanner.py
+-- tests
+-- README.md
+-- LICENSE
+-- main.py
```

### \_\_init__.py
```python
import json
import os

INDEX_FILE: str = "index.json"
LOGS_DIR: str = "logs"
LOGS_FILE: str = "medusa.log"
TMP_DIR: str = "tmp"

# Create empty index.json
if not os.path.exists(INDEX_FILE):
    with open(INDEX_FILE, "w") as index_file:
        index_file.write("{}")
...
```
This file ensures that upon starting the program, *tmp* and *logs/medusa.log* are created.

### database_communicator.py
```python
...
class DatabaseCommunicator:
    def __init__(self, dbname: str, default_table: str, user: str = "postgres",
                 host: str = "localhost", password: str = "") -> None:
        self._dbname = dbname
        self._host = host
        self._user = user
        self._password = password
        self._table = default_table
        self._conn = psycopg2.connect(dbname=dbname, user=user, host=host, password=password)
        self._cursor = self._conn.cursor(cursor_factory=DictCursor)

    @property
    def cursor(self):
        return self._cursor

    @property
    def table(self) -> str:
        return self._table
...
```
This class facilitates communication with a PostgreSQL database. The most important method is `match_hashes` as it's responsible for finding viruses.
```python
...
def match_hashes(self, files: list[dict], table: (str | None) = None) -> (list[str] | list[dict]):
        hashes: list[str] = [file["hash"] for file in files]
        update_log("Found hashes...<br>", timestamp=True)
        table = self.table if not table else table

        update_log("Matching hashes...<br>", timestamp=True)
        self.execute(f"SELECT hash FROM {table} WHERE hash IN ({self._parse_IN(hashes)})")
        update_log("Matched hashes!<br>", timestamp=True)

        matched_hashes: list[str] = [h[0] for h in self.cursor.fetchall()]
        viruses: list[str] = [file["name"] for file in files if file["hash"] in matched_hashes]
        viruses = list(set([find_archive(virus) for virus in viruses]))

        return [{}] if len(viruses) == 0 else viruses
```

### extract.py
```python
...
def unzip(path: str) -> list:
    try:
        if 0 < os.path.getsize(path) < 3e7 and is_zipfile(path):
            with ZipFile(path, "r") as opened_zip:
                tmp_path = f"./{TMP_DIR}/{path}-unzipped-medusa"
                update_log(f"Extracted into '{tmp_path}'.<br>", timestamp=True)
                opened_zip.extractall(tmp_path)
                return [f"{tmp_path}/{file}" for file in opened_zip.namelist() if not os.path.isdir(file)]
        else:
            return []
    except (BadZipFile, FileNotFoundError, OSError):
        return []
...
```
Untold until now, **extract.py** is very important to the accuracy of Medusa. Every discovered file in the scanning process is checked for *.zip* magic numbers. `unzip` and `runzip` functions are responsible for opening up these *zips* and instantiating `Files` of files inside. *Unzipping* process is recursive and will find every single file inside even a multiple times *zipped* archive.

### file.py
```python
...
class File:
    def __init__(self, path: str) -> None:
        self._relative_path: str = path
        self._raw_size: int = size(path)
        self._last_mod_posix: float = last_mod(path)

        # Hashes the file and returns the hash.
        if 0 < self._raw_size < 3e8:
            with open(path, "rb") as opened_file:
                # _Hash object creation.
                md5 = hashlib.md5()
                content = opened_file.read()
                md5.update(content)
                opened_file.close()
            self._hash: str = md5.hexdigest()
        else:
            self._hash: str = ""

    @property
    def name(self):
        return abspath(self._relative_path)
...
```
Responsible for hashing files and storing their information in `data`.
```python
...
@property
def data(self) -> dict:
	return {
	    "name": self.name,
	    "hash": self.hash,
	    "size": self.size,
	    "last_mod": self.last_mod,
	}
...
```

### scanner.py
```python
...
def file_rscan(directory: str) -> list[str]:
    discovered_files = []

    for dirpaths, dirs, files in os.walk(directory):
        # Checking for already exported zips.
        if not "-unzipped-medusa" in dirpaths:
            for file in files:
                path_to_file = f"{dirpaths}/{file}"
                update_log(f"Descended into '{path_to_file}'.<br>", timestamp=True)
                # Search inside zip archives.
                if is_zipfile(path_to_file):
                    discovered_files += runzip(path_to_file)
                else:
                    discovered_files.append(path_to_file)

    return discovered_files
...
```
*scanner.py* contains multiple functions that utilize `os.walk` and `threading.Thread` to quickly and recursively find all files inside **any** directory. Since the hashing process of `File` is long, *scanner.py* features multi-threaded hashing operations at the same time.
```python
...
def provision(directory: str, workers: int) -> Generator:
    """Provisions discovered files by file_rscan by the number of 'workers'."""
    files = file_rscan(directory)
    chunk_size: int = ceil(len(files) / workers)

    for index in range(0, len(files), chunk_size):
        yield files[index:index + chunk_size]

def scan(directory: str, threads: int = 1) -> list[dict]:
    """Launches 'threads' amount of concurrent hashing processes. Very fast and CPU intensive
    if used with a high amount of threads. Defaults to 1 thread. Takes into consideration index.json
    and thanks to that, it gets faster with every scan as the database builds up."""
    discovered_dicts: list = []
    snakes: list[Thread] = []

    def _discover_dicts(files: list[str]) -> None:
        for dict in cr_dicts(files):
            discovered_dicts.append(dict)

    def _update_and_return(discovered_dicts: list[dict[str, dict]]) -> Generator:
        multi_update_index(discovered_dicts)

        for dict in discovered_dicts:
            yield list(dict.values())[0]

    for file_list in provision(directory, threads):
        snakes.append(Thread(target=_discover_dicts, args=[file_list], daemon=True))

    for snake in snakes:
        snake.start()

    for snake in snakes:
        snake.join()

    return list(_update_and_return(discovered_dicts))
```
It divides discovered files in `file_rscan` into equal parts and then hashes the contents of each *sublist* in a different Thread.

### index.py and logger.py
```python
...
def update_index(content: dict[str, dict]) -> None:
    """Forceful updating of INDEX with a single entry"""
    INDEX.update(content)

    with open(INDEX_FILE, "w") as index_file:
        json.dump(INDEX, index_file, indent=4)
    update_log(f"Updated a single entry.<br>", timestamp=True)
...
```
> *index.py*

```python
...
def update_log(string: str, timestamp: bool = False):
    """Adds an entry to ./{LOGS_DIR}/{LOGS_FILE} in the format:
    [timestamp]     string

    timestamp is represented as HOUR/MINUTE/SECONDS"""
    if timestamp:
        text: str = datetime.now().strftime(f"[%H:%M:%S]\t") + string + "\n"
    else:
        text: str = string + "\n"

    with open(f"{LOGS_DIR}/{LOGS_FILE}", "a") as opened_log:
        opened_log.write(text)

def clear_log():
    """Clears content from ./{LOGS_DIR}/{LOGS_FILE}"""
    with open(f"{LOGS_DIR}/{LOGS_FILE}", "w") as opened_log:
        opened_log.write("")
```
> *logger.py*

Two rather unimportant files for understanding the workflow of Medusa. *index.py* is responsible for creating and updating **index.json** with new entries, while *logger.py* contains functions that allow appending into the **medusa.log**.

## License
This product is licensed under the GPLv3 as per PyQT5 requirement.

## Medusa doesn't work!
`main.py` will not work after *18.01.2023*. You have to use `main_backup.py`.